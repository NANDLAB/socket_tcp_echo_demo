/*
 ============================================================================
 Name        : socket_tcp_echo_client.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>

#define PORT 8080

ssize_t readall(int fd, void *buf, size_t n) {
	ssize_t res;
	size_t nread;
	for (nread = 0; nread < n; nread += res) {
		res = read(fd, buf + nread, n - nread);
		if (res == 0) { break; }
		if (res < 0) { return -1; }
	}
	return nread;
}

ssize_t writeall(int fd, const void *buf, size_t n) {
	ssize_t res;
	ssize_t nwrote;
	for (nwrote = 0; nwrote < n; nwrote += res) {
		res = write(fd, buf + nwrote, n - nwrote);
		if (res < 0) { return -1; }
	}
	return n;
}

int main(int argc, char *argv[]) {
	FILE *inbin = freopen(NULL, "rb", stdin);
	FILE *outbin = freopen(NULL, "wb", stdout);
	if (!inbin || !outbin) {
		fputs("Failed to set stdin or stdout to binary mode!\n", stderr);
		exit(EXIT_FAILURE);
	}
	const char *address = "127.0.0.1";
	if (argc >= 2) {
		address = argv[1];
	}
	struct sockaddr_in server = {
		.sin_family = AF_INET,
		.sin_port = htons(PORT),
		.sin_addr = (struct in_addr) {.s_addr = inet_addr(address)}
	};
	int client_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (client_fd == -1) {
		fputs("Could not create socket!\n", stderr);
		exit(EXIT_FAILURE);
	}
	fputs("Socket created successfully.\n", stderr);
	if (connect(client_fd, (struct sockaddr *) &server, sizeof(server)) < 0) {
		fputs("Connection error!\n", stderr);
		exit(EXIT_FAILURE);
	}
	fputs("Connected.\n", stderr);
	#define BUFFSIZE 1048576
	char out_buffer[BUFFSIZE] = {};
	char in_buffer[BUFFSIZE] = {};
	ssize_t ntosend;
	while (1) {
		ntosend = read(STDIN_FILENO, out_buffer, BUFFSIZE);
		if (ntosend < 0) {
			fputs("Stdin read error!\n", stderr);
			exit(EXIT_FAILURE);
		}
		else if (ntosend == 0) {
			fputs("Stdin EOF.\n", stderr);
			break;
		}
		else {
			fprintf(stderr, "%lu characters read from stdin.\n", ntosend);
			if (writeall(client_fd, out_buffer, ntosend) < 0) {
				fputs("Error writing data to socket! Perhaps the server disconnected.\n", stderr);
				exit(EXIT_FAILURE);
			}
			ssize_t res = readall(client_fd, in_buffer, ntosend);
			if (res < 0) {
				fputs("Error reading data from socket!\n", stderr);
				exit(EXIT_FAILURE);
			}
			else {
				if (writeall(STDOUT_FILENO, in_buffer, res) < 0) {
					fputs("Error writing out echo data to stdout!\n", stderr);
					exit(EXIT_FAILURE);
				}
				if (res < ntosend) {
					fputs("Server disconnected before sending back all the data!\n", stderr);
					exit(EXIT_FAILURE);
				}
			}
		}
	}
	close(client_fd);
	fputs("Socket closed.\n", stderr);
	fsync(STDIN_FILENO);
	fsync(STDOUT_FILENO);
	fsync(STDERR_FILENO);
	return EXIT_SUCCESS;
}
